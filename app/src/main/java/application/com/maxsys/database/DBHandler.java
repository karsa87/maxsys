package application.com.maxsys.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;

import application.com.maxsys.handler.Laporan;
import application.com.maxsys.handler.Outlet;
import application.com.maxsys.handler.Product;

/**
 * Created by KARSA on 03/06/2016.
 */
public class DBHandler extends SQLiteOpenHelper {
    private final static String TAG = "DB";
    // Database Version
    private static final int DATABASE_VERSION = 1;
    public static long id;

    // Database Name
    public static final String DATABASE_NAME = "maxsys";

    private HashMap<String, String> _table;

    public DBHandler(Context context, HashMap<String, String> table) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        _table = table;
        Log.i(TAG, "Loading database...");
    }

    public DBHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        Log.i(TAG, "Loading database...");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        if(_table != null){
            for (String table : _table.keySet()) {
                Log.i(TAG, "Create Table : "+table);
                String query = _table.get(table);
                db.execSQL(query);
            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(_table != null){
            for (String table : _table.keySet()) {
                Log.i(TAG, "Drop Table : "+table);
                db.execSQL("DROP TABLE IF EXISTS " + table);
            }
        }

        onCreate(db);
    }

    public void insert(String TABLE_NAME, ContentValues values){
        SQLiteDatabase db = this.getWritableDatabase();

        // Inserting Row
        id = db.insert(TABLE_NAME, null, values);
        db.close(); // Closing database connection
    }

    public HashMap<String, String> select(String TABLE_NAME, String[] column, String whereClause){
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, column, whereClause, null, null, null, null, null);
        HashMap<String, String> data = new HashMap<>();

        if (cursor != null){
            if(cursor.moveToFirst()){
                for(int i=0; i<column.length; i++){
                    data.put(column[i], cursor.getString(i));
                }
            }
        }else{
            return null;
        }

        return data;
    }

    public ArrayList<HashMap<String, String>> selectList(String TABLE_NAME, String[] column){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<HashMap<String, String>> arrayData = new ArrayList<>();

        Cursor cursor = db.rawQuery("SELECT * FROM "+TABLE_NAME, null);

        if (cursor.moveToFirst()){
            do {
                HashMap<String, String> data = new HashMap<>();
                for(int i=0; i<column.length; i++){
                    data.put(column[i], cursor.getString(i));
                }

                arrayData.add(data);
            }while (cursor.moveToNext());
        }

        return arrayData;
    }

    public ArrayList<HashMap<String, String>> selectList(String TABLE_NAME, String[] column, String whereClause){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<HashMap<String, String>> arrayData = new ArrayList<>();

        StringBuilder query = new StringBuilder();
        query.append("SELECT * FROM "+TABLE_NAME);
        if(!("".equalsIgnoreCase(whereClause) || whereClause == null)){
            query.append(" WHERE "+whereClause);
        }
        Cursor cursor = db.rawQuery(query.toString(), null);

        if (cursor.moveToFirst()){
            do {
                HashMap<String, String> data = new HashMap<>();
                for(int i=0; i<column.length; i++){
                    data.put(column[i], cursor.getString(i));
                }

                arrayData.add(data);
            }while (cursor.moveToNext());
        }

        return arrayData;
    }

    public ArrayList<HashMap<String, String>> selectList(String[] column, String query){
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<HashMap<String, String>> arrayData = new ArrayList<>();

        Cursor cursor = db.rawQuery(query, null);

        if (cursor.moveToFirst()){
            do {
                HashMap<String, String> data = new HashMap<>();
                for(int i=0; i<column.length; i++){
                    data.put(column[i], cursor.getString(i));
                }

                arrayData.add(data);
            }while (cursor.moveToNext());
        }

        return arrayData;
    }

    public int update(String TABLE_NAME, ContentValues values, String whereClause){
        SQLiteDatabase db = this.getWritableDatabase();

        // updating row
        int hasil = db.update(TABLE_NAME, values, whereClause, null);

        return hasil;
    }

    /**
     * delete id row of table
     */
    public boolean delete(String table, String where) {
        SQLiteDatabase db = this.getWritableDatabase();
        long index = db.delete(table, where, null);
        close();
        return index > 0;
    }

    public boolean deleteLaporan(String where){
        return delete(Laporan.TABLE_NAME, where);
    }

    public boolean deleteOutler(String where){
        return delete(Outlet.TABLE_NAME, where);
    }

    public boolean deleteProduct(String where){
        return delete(Product.TABLE_NAME, where);
    }
}
