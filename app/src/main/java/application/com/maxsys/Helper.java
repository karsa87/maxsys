package application.com.maxsys;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by KARSA on 31/05/2016.
 */
public class Helper {
    public static String formatDate(String format, Date date){
        DateFormat dateFormat = new SimpleDateFormat(format);

        return dateFormat.format(date);
    }

    public static Date convertToDate(String format, String date){
        DateFormat dateFormat = new SimpleDateFormat(format);

        try {
            return dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

    public static String changeFormatCurrency(Double amount){
        String result = String.format("%,.0f", amount);

        result = result.replace(",",".");
        return result;
    }

    public static Date addDay(Date date, int plusDay){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, plusDay);

        return c.getTime();
    }
}
