package application.com.maxsys.handler;

import android.content.ContentValues;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import application.com.maxsys.Helper;
import application.com.maxsys.database.DBHandler;

/**
 * Created by KARSA on 03/06/2016.
 */
public class Laporan {
    public final static String TABLE_NAME = "e_laporan";
    public final static String KEY_ID = "e_laporan_id";
    public final static String KEY_HPP = "hpp";
    public final static String KEY_LABA_RUGI = "laba_rugi";
    public final static String KEY_PRODUCT_ID = "m_product_id";
    public final static String KEY_OUTLET_ID = "t_outlet_id";
    public final static String KEY_CREATE_DATE = "create_date";
    public final static String KEY_UPDATE_DATE = "update_date";
    public final static String KEY_ISACTIVE = "isactive";
    public final static String KEY_STOK = "stok";

    ArrayList<HashMap<String, String>> listData;
    ArrayList<String> listNama = new ArrayList<>();

    int hpp, labaRugi, stok;

    public static String addTable (){
        return "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_HPP + " REAL ,"
                + KEY_LABA_RUGI + " REAL ,"
                + KEY_PRODUCT_ID + " INTEGER ,"
                + KEY_OUTLET_ID + " INTEGER ,"
                + KEY_STOK + " INTEGER,"
                + KEY_CREATE_DATE + " NUMERIC ,"
                + KEY_UPDATE_DATE + " NUMERIC,"
                + KEY_ISACTIVE + " NUMERIC"
                + ")";
    }

    public static String[] allColumn(){
        return new String[]{KEY_ID, KEY_HPP, KEY_LABA_RUGI, KEY_PRODUCT_ID, KEY_OUTLET_ID, KEY_STOK, KEY_CREATE_DATE, KEY_UPDATE_DATE, KEY_ISACTIVE};
    }

    public static ContentValues createQuery(int hpp, int harga1, int stok ,Date createDate, Date updateDate, int productId, long outletId, boolean isactive){
        /*********
         * Hanya untuk generate query untuk record baru
         * *********/

        ContentValues values = new ContentValues();
        values.put(KEY_HPP, hpp);
        values.put(KEY_LABA_RUGI, harga1);
        values.put(KEY_PRODUCT_ID, productId);
        values.put(KEY_OUTLET_ID, outletId);
        values.put(KEY_STOK, stok);
        values.put(KEY_CREATE_DATE, Helper.formatDate("yyyy-MM-dd", createDate));
        values.put(KEY_UPDATE_DATE, Helper.formatDate("yyyy-MM-dd", updateDate));
        values.put(KEY_ISACTIVE, isactive);

        return  values;
    }

    public static int calculateHpp(int stok, int hpp){
        return  stok * hpp;
    }

    public static int calculateProfit(int stok, int hpp, int harga){
        return stok * (harga - hpp);
    }

    public void parse(ArrayList<HashMap<String, String>> data){
        ArrayList<String> dataArray = new ArrayList<>();
        listData = data;

        for (int i=0; i<data.size(); i++){
            HashMap<String, String> row = data.get(i);
            String outletName = row.get(Outlet.KEY_NAMA);
            dataArray.add(outletName);
        }

        listNama = dataArray;
    }

    public void parse(HashMap<String, String> data){
        hpp = Integer.parseInt(data.get(KEY_HPP));
        labaRugi = Integer.parseInt(data.get(KEY_LABA_RUGI));
        stok = Integer.parseInt(data.get(KEY_STOK));
    }

    public ArrayList<String> getListNama() {
        return listNama;
    }

    public ArrayList<HashMap<String, String>> getListData() {
        return listData;
    }

    public int getHpp() {
        return hpp;
    }

    public int getLabaRugi() {
        return labaRugi;
    }

    public int getStok() {
        return stok;
    }
}
