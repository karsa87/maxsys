package application.com.maxsys.handler;

import android.content.ContentValues;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import application.com.maxsys.Helper;

/**
 * Created by KARSA on 03/06/2016.
 */
public class Product {
    public final static String TABLE_NAME = "m_product";
    public final static String KEY_ID = "m_product_id";
    public final static String KEY_KODE = "kode";
    public final static String KEY_NAMA = "nama";
    public final static String KEY_HPP = "hpp";
    public final static String KEY_HARGA1 = "harga1";
    public final static String KEY_HARGA2 = "harga2";
    public final static String KEY_HARGA3 = "harga3";
    public final static String KEY_STOK = "stok";
    public final static String KEY_CREATE_DATE = "create_date";
    public final static String KEY_UPDATE_DATE = "update_date";

    public final static int PRODUCT_ID = 1;

    private String id, kode, nama;
    private Date createDate, updateDate;
    private int hpp, harga1, harga2, harga3, stok;

    public static String addTable (){
        return "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_KODE + " TEXT ,"
                + KEY_NAMA + " TEXT ,"
                + KEY_HPP + " REAL ,"
                + KEY_HARGA1 + " REAL ,"
                + KEY_HARGA2 + " REAL ,"
                + KEY_HARGA3 + " REAL ,"
                + KEY_STOK + " INTEGER ,"
                + KEY_CREATE_DATE + " NUMERIC ,"
                + KEY_UPDATE_DATE + " NUMERIC"
                + ")";
    }

    public static ContentValues createQuery(String kode, String nama, int hpp, int harga1, int harga2
        , int harga3, int stok, Date createDate, Date updateDate){
        /*********
         * Hanya untuk generate query untuk record baru
         * *********/

        ContentValues values = new ContentValues();
        values.put(KEY_ID, PRODUCT_ID);
        values.put(KEY_KODE, kode);
        values.put(KEY_NAMA, nama);
        values.put(KEY_HPP, hpp);
        values.put(KEY_HARGA1, harga1);
        values.put(KEY_HARGA2, harga2);
        values.put(KEY_HARGA3, harga3);
        values.put(KEY_STOK, stok);
        values.put(KEY_CREATE_DATE, Helper.formatDate("yyyy-MM-dd", createDate));
        values.put(KEY_UPDATE_DATE, Helper.formatDate("yyyy-MM-dd", updateDate));

        return  values;
    }

    public static String[] allColumn(){
        return new String[]{KEY_ID,KEY_KODE,KEY_NAMA,KEY_HPP,KEY_HARGA1,KEY_HARGA2,KEY_HARGA3,KEY_STOK,KEY_CREATE_DATE,KEY_UPDATE_DATE};
    }

    public void parse(HashMap<String, String> data){
        id = data.get(KEY_ID);
        kode = data.get(KEY_KODE);
        nama = data.get(KEY_NAMA);
        hpp = Integer.parseInt(data.get(KEY_HPP));
        harga1 = Integer.parseInt(data.get(KEY_HARGA1));
        harga2 = Integer.parseInt(data.get(KEY_HARGA2));
        harga3 = Integer.parseInt(data.get(KEY_HARGA3));
        stok = Integer.parseInt(data.get(KEY_STOK));
        createDate = Helper.convertToDate("yyyy-MM-dd", data.get(KEY_CREATE_DATE));
        updateDate = Helper.convertToDate("yyyy-MM-dd", data.get(KEY_UPDATE_DATE));
    }

    public static ArrayList<String> convertCostList(HashMap<String, String> listData){
        ArrayList<String> list = new ArrayList<>();
        list.add(listData.get(KEY_HARGA1));
        list.add(listData.get(KEY_HARGA2));
        list.add(listData.get(KEY_HARGA3));

        return  list;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public int getHarga1() {
        return harga1;
    }

    public int getHarga2() {
        return harga2;
    }

    public int getHarga3() {
        return harga3;
    }

    public int getHpp() {
        return hpp;
    }

    public int getStok() {
        return stok;
    }

    public String getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getKode() {
        return kode;
    }
}
