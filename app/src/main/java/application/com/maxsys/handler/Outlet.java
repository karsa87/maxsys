package application.com.maxsys.handler;

import android.content.ContentValues;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import application.com.maxsys.Helper;

/**
 * Created by KARSA on 03/06/2016.
 */
public class Outlet {
    public final static String TABLE_NAME = "t_outlet";
    public final static String KEY_ID = "t_outlet_id";
    public final static String KEY_NAMA = "nama";
    public final static String KEY_ALAMAT = "alamat";
    public final static String KEY_TELP = "telp";
    public final static String KEY_HARGA = "harga";
    public final static String KEY_INDEX_HARGA = "index_harga";
    public final static String KEY_STOK = "stok";
    public final static String KEY_STOK_RETUR = "stok_retur";
    public final static String KEY_PRODUCT_ID ="m_product_id";
    public final static String KEY_ISACTIVE = "isactive";
    public final static String KEY_CREATE_DATE = "create_date";
    public final static String KEY_UPDATE_DATE = "update_date";

    ArrayList<HashMap<String, String>> _listData = new ArrayList<>();
    ArrayList<String> _listNama = new ArrayList<>();
    String nama, alamat, telp;
    int id, harga, stok, productId, indexHarga, stokRetur;
    boolean isactive;
    Date createDate, updateDate;

    public static String addTable (){
        return "CREATE TABLE " + TABLE_NAME + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                + KEY_NAMA + " TEXT ,"
                + KEY_ALAMAT + " TEXT ,"
                + KEY_TELP + " TEXT ,"
                + KEY_INDEX_HARGA + " TEXT ,"
                + KEY_HARGA + " REAL ,"
                + KEY_STOK + " INTEGER ,"
                + KEY_STOK_RETUR + " INTEGER ,"
                + KEY_PRODUCT_ID + " INTEGER ,"
                + KEY_ISACTIVE + " NUMERIC,"
                + KEY_CREATE_DATE + " NUMERIC ,"
                + KEY_UPDATE_DATE + " NUMERIC"
                + ")";
    }

    public static String[] allColumn(){
        return new String[]{KEY_ID, KEY_NAMA, KEY_ALAMAT, KEY_TELP, KEY_INDEX_HARGA, KEY_HARGA, KEY_STOK, KEY_STOK_RETUR, KEY_PRODUCT_ID, KEY_ISACTIVE, KEY_CREATE_DATE, KEY_UPDATE_DATE};
    }

    public void parse(ArrayList<HashMap<String, String>> data){
        ArrayList<String> listData = new ArrayList<>();
        _listData = data;

        for (int i=0; i<data.size(); i++){
            HashMap<String, String> row = data.get(i);
            listData.add(row.get(KEY_NAMA));
        }

        _listNama = listData;
    }

    public void parse(HashMap<String,String> data){
        id = Integer.parseInt(data.get(KEY_ID));
        nama = data.get(KEY_NAMA);
        alamat = data.get(KEY_ALAMAT);
        telp = data.get(KEY_TELP);
        indexHarga = Integer.parseInt(data.get(KEY_INDEX_HARGA));
        harga = Integer.parseInt(data.get(KEY_HARGA));
        stok = Integer.parseInt(data.get(KEY_STOK));
        if("".equalsIgnoreCase(data.get(KEY_STOK_RETUR)) || data.get(KEY_STOK_RETUR) == null){
            stokRetur = 0;
        }else{
            stokRetur = Integer.parseInt(data.get(KEY_STOK_RETUR));
        }
        productId = Integer.parseInt(data.get(KEY_PRODUCT_ID));
        if("1".equalsIgnoreCase(data.get(KEY_ISACTIVE))){
            isactive = true;
        }else {
            isactive = false;
        }

        createDate = Helper.convertToDate("yyyy-MM-dd", data.get(KEY_CREATE_DATE));
        updateDate = Helper.convertToDate("yyyy-MM-dd", data.get(KEY_UPDATE_DATE));
    }

    public ArrayList<HashMap<String, String>> get_listData() {
        return _listData;
    }

    public ArrayList<String> get_listNama() {
        return _listNama;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getStok() {
        return stok;
    }

    public void setStok(int stok) {
        this.stok = stok;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getHarga() {
        return harga;
    }

    public void setHarga(int harga) {
        this.harga = harga;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTelp() {
        return telp;
    }

    public void setTelp(String telp) {
        this.telp = telp;
    }

    public boolean getIsActive(){
        return isactive;
    }

    public void setIsactive(boolean isactive) {
        this.isactive = isactive;
    }

    public int getIndexHarga() {
        return indexHarga;
    }

    public void setIndexHarga(int indexHarga) {
        this.indexHarga = indexHarga;
    }

    public int getStokRetur() {
        return stokRetur;
    }

    public void setStokRetur(int stokRetur) {
        this.stokRetur = stokRetur;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Date getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(Date updateDate) {
        this.updateDate = updateDate;
    }

    public static ContentValues createContentValue(String nama, String alamat, String telp, int hargaPosition,
                                            String harga, String stok, String stokReturn, boolean isactive, int productId,
                                            Date createDate, Date updateDate){
        ContentValues values = new ContentValues();
        values.put(Outlet.KEY_NAMA, nama);
        values.put(Outlet.KEY_ALAMAT, alamat);
        values.put(Outlet.KEY_TELP, telp);
        values.put(Outlet.KEY_INDEX_HARGA, hargaPosition);
        values.put(Outlet.KEY_HARGA, harga);
        values.put(Outlet.KEY_STOK, stok);
        values.put(Outlet.KEY_STOK_RETUR, stokReturn);
        values.put(Outlet.KEY_ISACTIVE, isactive);
        values.put(Outlet.KEY_PRODUCT_ID, productId);
        values.put(Outlet.KEY_CREATE_DATE, Helper.formatDate("yyyy-MM-dd", createDate));
        values.put(Outlet.KEY_UPDATE_DATE, Helper.formatDate("yyyy-MM-dd", updateDate));

        return values;
    }
}


