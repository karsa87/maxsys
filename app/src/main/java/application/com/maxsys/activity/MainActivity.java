package application.com.maxsys.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import application.com.maxsys.R;
import application.com.maxsys.database.BackupData;
import application.com.maxsys.database.DBHandler;
import application.com.maxsys.handler.Laporan;
import application.com.maxsys.handler.Outlet;
import application.com.maxsys.handler.Product;
import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity {
    @Bind(R.id.btn_laporan)
    Button _laporanBtn;
    @Bind(R.id.btn_outlet)
    Button _outletBtn;
    @Bind(R.id.btn_product)
    Button _productBtn;
    @Bind(R.id.btn_service)
    Button _serviceBtn;
    @Bind(R.id.text_stock)
    TextView _stokText;

    private final static int REQUEST_PRODUCT = 0;
    private final static int REQUEST_OUTLET = 1;
    private final static int REQUEST_LAPORAN = 2;
    private final static int REQUEST_SERVICE = 3;

    private BackupData backupData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        _laporanBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLaporan();
            }
        });

        _outletBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOutlet();
            }
        });

        _productBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showProduct();
            }
        });

        _serviceBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showService();
            }
        });

        loadStok();

        backupData = new BackupData(this);
        backupData.setOnBackupListener(new BackupData.OnBackupListener() {
            @Override
            public void onFinishExport(String error) {
                String notify = error;
                if (error == null) {
                    notify = "Export success";
                }
                Toast.makeText(MainActivity.this, notify, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFinishImport(String error) {
                String notify = error;
                if (error == null) {
                    notify = "Import success";
                    loadStok();
                }
                Toast.makeText(MainActivity.this, notify, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void showLaporan(){
        Intent intent = new Intent(this, LaporanActivity.class);
        startActivityForResult(intent, REQUEST_LAPORAN);
    }

    public void showOutlet(){
        if(loadStok()){
            Intent intent = new Intent(this,OutletActivity.class);
            startActivityForResult(intent, REQUEST_OUTLET);
        }else {
            Toast.makeText(MainActivity.this, "Anda belum mempunyai product, silakan buat produk lebih dahulu", Toast.LENGTH_SHORT).show();
        }
    }

    public void showProduct(){
        Intent intent = new Intent(this, ProductActivity.class);
        startActivityForResult(intent, REQUEST_PRODUCT);
    }

    public void showService(){
        if(loadStok()){
            Intent intent = new Intent(this, ServiceActivity.class);
            startActivityForResult(intent, REQUEST_SERVICE);
        }else {
            Toast.makeText(MainActivity.this, "Anda belum mempunyai product, silakan buat produk lebih dahulu", Toast.LENGTH_SHORT).show();
        }
    }

    private boolean loadStok(){
        DBHandler db = new DBHandler(this);
        HashMap<String, String> data = db.select(Product.TABLE_NAME, Product.allColumn(), Product.KEY_ID + "=" + Product.PRODUCT_ID);
        if(data.size() > 0){
            _stokText.setText(data.get(Product.KEY_STOK)+" pcs");
            return true;
        }else{
            _stokText.setText("anda tidak memiliki stok produk");
            return false;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        loadStok();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_backup:
                backupData.exportToSD();
                break;
            case R.id.menu_import:
                backupData.importFromSD();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
