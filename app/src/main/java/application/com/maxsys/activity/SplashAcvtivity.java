package application.com.maxsys.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import application.com.maxsys.R;
import application.com.maxsys.database.DBHandler;
import application.com.maxsys.handler.Laporan;
import application.com.maxsys.handler.Outlet;
import application.com.maxsys.handler.Product;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by KARSA on 03/06/2016.
 */
public class SplashAcvtivity extends AppCompatActivity {
    long millisInFuture = 3 * 1000; // 3 detik
    long countDownInterval = 1000; // 1 detik
    CountDownTimer timer;

    @Bind(R.id.layout_splash)
    LinearLayout _splashLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        timer = new CountDownTimer(millisInFuture,countDownInterval){
            public void onTick(long millisUntilFinished){
                long millis = millisUntilFinished;

                String hms = String.format(" %d days ", TimeUnit.MILLISECONDS.toDays(millis));
            }
            public void onFinish(){
                Intent intent = new Intent(SplashAcvtivity.this, MainActivity.class);
                startActivity(intent);
            }
        }.start();

        createTable();
    }

    private void createTable(){
        HashMap<String, String> table = new HashMap<>();
        table.put(Product.TABLE_NAME, Product.addTable());
        table.put(Outlet.TABLE_NAME, Outlet.addTable());
        table.put(Laporan.TABLE_NAME, Laporan.addTable());

        DBHandler db = new DBHandler(this, table);
        db.getWritableDatabase();
        db.getReadableDatabase();
    }
}
