package application.com.maxsys.activity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Date;
import java.util.HashMap;

import application.com.maxsys.Helper;
import application.com.maxsys.R;
import application.com.maxsys.database.DBHandler;
import application.com.maxsys.handler.Product;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by karsa on 02/06/16.
 */
public class ProductActivity extends AppCompatActivity {
    @Bind(R.id.btn_ok)
    Button _okBtn;
    @Bind(R.id.btn_cancel)
    Button _cancelBtn;
    @Bind(R.id.input_kode)
    EditText _kodeInput;
    @Bind(R.id.input_harga1)
    EditText _harga1Input;
    @Bind(R.id.input_harga2)
    EditText _harga2Input;
    @Bind(R.id.input_harga3)
    EditText _harga3Input;
    @Bind(R.id.input_nama_produk)
    EditText _namaInput;
    @Bind(R.id.input_hpp)
    EditText _hppInput;
    @Bind(R.id.input_stok)
    EditText _stokInput;

    ProgressDialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _okBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alert = new AlertDialog.Builder(ProductActivity.this);
                alert.setTitle("Warning");
                alert.setMessage("Apakah anda yakin untuk update data produk?");
                alert.setPositiveButton(getString(R.string.btn_ya), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(validate()){
                            CreateProduct createProduct = new CreateProduct();
                            createProduct.execute();
                        }
                    }
                });

                alert.setNegativeButton(getString(R.string.btn_tidak), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alert.show();
            }
        });

        _cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        GetProduct createAsyncTask = new GetProduct();
        createAsyncTask.execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case android.R.id.home:
                finish();
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    class GetProduct extends AsyncTask<String, Void, HashMap<String, String>>{

        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(ProductActivity.this);
            dialog.setMessage("Memuat data...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected HashMap<String, String> doInBackground(String... params) {
            //insert data product
            DBHandler database = new DBHandler(ProductActivity.this);
            HashMap<String,String> data;
            data = database.select(Product.TABLE_NAME, Product.allColumn(), Product.KEY_ID + "='" + Product.PRODUCT_ID + "'");
            if(data == null || data.size() <= 0){
                database.insert(Product.TABLE_NAME, Product.createQuery("", "", 0, 0, 0, 0, 0, new Date(), new Date()));
                data = database.select(Product.TABLE_NAME, Product.allColumn(), Product.KEY_ID + "=" + Product.PRODUCT_ID);
            }

            return data;
        }

        @Override
        protected void onPostExecute(HashMap<String, String> result) {
            setData(result);
            dialog.dismiss();
        }
    }

    class CreateProduct extends AsyncTask<String, Void, String>{
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(ProductActivity.this);
            dialog.setMessage("Memuat data...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            updateData();

            return "";
        }

        @Override
        protected void onPostExecute(String result) {
            Toast.makeText(ProductActivity.this, "Berhasil disimpan", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
        }
    }

    private void setData(HashMap<String, String> data){
        Product product = new Product();
        product.parse(data);
        _kodeInput.setText(product.getKode());
        _namaInput.setText(product.getNama());
        _hppInput.setText(String.valueOf(product.getHpp()));
        _harga1Input.setText(String.valueOf(product.getHarga1()));
        _harga2Input.setText(String.valueOf(product.getHarga2()));
        _harga3Input.setText(String.valueOf(product.getHarga3()));
        _stokInput.setText(String.valueOf(product.getStok()));
    }

    private void updateData(){
        DBHandler database = new DBHandler(ProductActivity.this);

        ContentValues value = new ContentValues();
        value.put(Product.KEY_NAMA, _namaInput.getText().toString());
        value.put(Product.KEY_KODE, _kodeInput.getText().toString());
        value.put(Product.KEY_HPP, _hppInput.getText().toString());
        value.put(Product.KEY_HARGA1, _harga1Input.getText().toString());
        value.put(Product.KEY_HARGA2, _harga2Input.getText().toString());
        value.put(Product.KEY_HARGA3, _harga3Input.getText().toString());
        value.put(Product.KEY_STOK, _stokInput.getText().toString());
        value.put(Product.KEY_CREATE_DATE, Helper.formatDate("yyyy-MM-dd", new Date()));
        value.put(Product.KEY_UPDATE_DATE, Helper.formatDate("yyyy-MM-dd", new Date()));

        database.update(Product.TABLE_NAME, value, Product.KEY_ID + "=" + Product.PRODUCT_ID);
    }

    private boolean validate(){
        boolean result = true;

        String hpp = _hppInput.getText().toString();
        String harga1 = _harga1Input.getText().toString();
        String harga2 = _harga2Input.getText().toString();
        String harga3 = _harga3Input.getText().toString();
        String stok = _stokInput.getText().toString();

        if(hpp.isEmpty() || "0".equalsIgnoreCase(hpp) || "".equalsIgnoreCase(hpp)){
            _hppInput.setError("Hpp harus diissi");
            result = false;
        }

        if(harga1.isEmpty() || "0".equalsIgnoreCase(harga1) || "".equalsIgnoreCase(harga1)){
            _harga1Input.setError("Harga satu harus diisi");
            if(!result){
                result = false;
            }
        }else{
            int harga1Int = Integer.parseInt(harga1);
            int hppInt = Integer.parseInt(hpp);

            if(harga1Int < hppInt){
                _harga1Input.setError("Harga satu tidak boleh lebih rendah dari HPP");
                if(!result){
                    result = false;
                }
            }
        }

        if(stok.isEmpty() || "0".equalsIgnoreCase(stok) || "".equalsIgnoreCase(stok)){
            _stokInput.setError("Stok harus diisi");
            if(!result){
                result = false;
            }
        }

        if(!harga2.isEmpty()){
            int harga2Int = Integer.parseInt(harga2);
            int hppInt = Integer.parseInt(hpp);

            if(harga2Int < hppInt){
                _harga2Input.setError("Harga dua tidak boleh lebih rendah dari HPP");
                if(!result){
                    result = false;
                }
            }
        }

        if(!harga3.isEmpty()){
            int harga3Int = Integer.parseInt(harga2);
            int hppInt = Integer.parseInt(hpp);

            if(harga3Int < hppInt){
                _harga3Input.setError("Harga tiga tidak boleh lebih rendah dari HPP");
                if(!result){
                    result = false;
                }
            }
        }

        return result;
    }
}
