package application.com.maxsys.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import application.com.maxsys.Helper;
import application.com.maxsys.R;
import application.com.maxsys.database.DBHandler;
import application.com.maxsys.handler.Laporan;
import application.com.maxsys.handler.Outlet;
import application.com.maxsys.handler.Product;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by KARSA on 02/06/2016.
 */
public class LaporanActivity extends AppCompatActivity {
    @Bind(R.id.listView_laporan)
    ListView _laporanListView;
    @Bind(R.id.text_stock)
    TextView _stockText;
    @Bind(R.id.text_labarugi)
    TextView _labaRugiText;

    private DatePickerDialog dialogDateFrom;
    private DatePickerDialog dialogDateTo;
    private ProgressDialog dialog;
    private Menu menu;

    Laporan laporan;

    String sumStok = "0";
    String sumLabaRugi = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_laporan);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _laporanListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<HashMap<String,String>> listData = laporan.getListData();
                HashMap<String,String> data = listData.get(position);
                laporan.parse(data);

                AlertDialog.Builder alert = new AlertDialog.Builder(LaporanActivity.this);
                alert.setTitle("Information");

                LayoutInflater inflater = (LayoutInflater) LaporanActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View layout = inflater.inflate(R.layout.popup_laporan, (ViewGroup) findViewById(R.id.popup_laporan));

                alert.setView(layout);

                EditText hpp = (EditText) layout.findViewById(R.id.input_hpp);
                EditText labaRugi = (EditText) layout.findViewById(R.id.input_laba_rugi);
                EditText stok = (EditText) layout.findViewById(R.id.input_stok);
                hpp.setText(String.valueOf(laporan.getHpp()));
                labaRugi.setText(String.valueOf(laporan.getLabaRugi()));
                stok.setText(String.valueOf(laporan.getStok()));

                alert.show();
            }
        });

        GetLaporan getLaporan = new GetLaporan();
        getLaporan.execute();
    }

    private void setData(final EditText _dateFrom, final EditText _dateTo){
        Calendar calendar = Calendar.getInstance();

        dialogDateFrom = new DatePickerDialog(LaporanActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                String date = Helper.formatDate("yyyy-MM-dd",newDate.getTime());
                _dateFrom.setText(date);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        dialogDateTo = new DatePickerDialog(LaporanActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                String date = Helper.formatDate("yyyy-MM-dd",newDate.getTime());
                _dateTo.setText(date);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case android.R.id.home:
                finish();
                break;
            case R.id.btn_filter:
                showDateDialog();
                break;
            case R.id.btn_empty_filter:
                GetLaporan getLaporan = new GetLaporan();
                getLaporan.execute();
                menu.getItem(0).setVisible(false);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_laporan, menu);
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    private void showDateDialog(){
        LayoutInflater inflater = (LayoutInflater) LaporanActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_filter, (ViewGroup) findViewById(R.id.popup_filter));

        AlertDialog.Builder alert = new AlertDialog.Builder(LaporanActivity.this);
        alert.setView(layout);

        final EditText _dateFromInput = (EditText) layout.findViewById(R.id.input_date_from);
        final EditText _dateToInput = (EditText) layout.findViewById(R.id.input_date_to);
        Button _dateToBtn = (Button) layout.findViewById(R.id.btn_date_to);
        Button _dateFromBtn = (Button) layout.findViewById(R.id.btn_date_from);

        setData(_dateFromInput, _dateToInput);

        _dateFromBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDateFrom.show();
            }
        });

        _dateToBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDateTo.show();
            }
        });

        alert.setPositiveButton(R.string.btn_filter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (validate(_dateFromInput, _dateToInput)) {
                    menu.getItem(0).setVisible(true);

                    FilterLaporan filter = new FilterLaporan();
                    filter.execute(new String[]{_dateFromInput.getText().toString(), _dateToInput.getText().toString()});
                } else {
                    Toast.makeText(LaporanActivity.this, "Filter anda gagal mohon diulangi", Toast.LENGTH_SHORT).show();
                }
            }
        });

        alert.setNegativeButton(R.string.btn_tidak, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.show();
    }

    private boolean validate(EditText _dateFrom, EditText _dateTo){
        boolean result = true;
        String dateFrom = _dateFrom.getText().toString();
        String dateTo = _dateTo.getText().toString();

        if(dateFrom.isEmpty() || "".equalsIgnoreCase(dateFrom)){
            _dateFrom.setError("Date from harus diisi");
            result = false;
        }

        if(dateTo.isEmpty() || "".equalsIgnoreCase(dateTo)){
            _dateTo.setError("Date from harus diisi");
            if(!result){
                result = false;
            }
        }

        return result;
    }

    class GetLaporan extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>> {
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(LaporanActivity.this);
            dialog.setMessage("Memuat list...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected ArrayList<HashMap<String,String>> doInBackground(String... params) {
            DBHandler db = new DBHandler(LaporanActivity.this);

            // mencari yang outlet yang aktif saja
            ArrayList<HashMap<String,String>> data = db.selectList(new String[]{Laporan.KEY_HPP, Laporan.KEY_LABA_RUGI, Laporan.KEY_UPDATE_DATE, Outlet.KEY_NAMA, Laporan.KEY_STOK} ,
                    "SELECT l."+Laporan.KEY_HPP+", l."+Laporan.KEY_LABA_RUGI+", l."+Laporan.KEY_UPDATE_DATE+", o."+ Outlet.KEY_NAMA+", l."+Laporan.KEY_STOK
                            +" FROM "+Laporan.TABLE_NAME +" as l"
                            +" INNER JOIN "+ Outlet.TABLE_NAME+" as o ON o."+Outlet.KEY_ID+"=l."+Laporan.KEY_OUTLET_ID+" "
                            +" WHERE l."+Laporan.KEY_ISACTIVE+" = 1 AND o."+Outlet.KEY_ISACTIVE+"=1"
                            +" ORDER BY l."+Laporan.KEY_CREATE_DATE);

            getSum(params, db);

            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String,String>> data) {
            setDataList(data);
            dialog.dismiss();
        }
    }

    class FilterLaporan extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>> {
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(LaporanActivity.this);
            dialog.setMessage("Memuat list...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected ArrayList<HashMap<String,String>> doInBackground(String... params) {
            DBHandler db = new DBHandler(LaporanActivity.this);

            // mencari yang outlet yang aktif saja
            ArrayList<HashMap<String,String>> data = db.selectList(new String[]{Laporan.KEY_HPP, Laporan.KEY_LABA_RUGI, Laporan.KEY_UPDATE_DATE, Outlet.KEY_NAMA, Laporan.KEY_STOK} ,
                    "SELECT l."+Laporan.KEY_HPP+", l."+Laporan.KEY_LABA_RUGI+", l."+Laporan.KEY_UPDATE_DATE+", o."+ Outlet.KEY_NAMA+", l."+Laporan.KEY_STOK
                            +" FROM "+Laporan.TABLE_NAME +" as l"
                            +" INNER JOIN "+ Outlet.TABLE_NAME+" as o ON o."+Outlet.KEY_ID+"=l."+Laporan.KEY_OUTLET_ID+" "
                            +" WHERE l."+Laporan.KEY_CREATE_DATE +" >= '"+params[0]+"' and l."+Laporan.KEY_CREATE_DATE +" <= '"+params[1]+"'"
                            +" AND l."+Laporan.KEY_ISACTIVE+" = 1 AND o."+Outlet.KEY_ISACTIVE+"=1"
                            +" ORDER BY l."+Laporan.KEY_CREATE_DATE);

            getSum(params, db);
            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String,String>> data){
            setDataList(data);
            dialog.dismiss();
        }
    }

    private void setDataList(ArrayList<HashMap<String,String>> data){
        _stockText.setText(sumStok);
        _labaRugiText.setText(sumLabaRugi);

        laporan = new Laporan();
        laporan.parse(data);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1, laporan.getListNama());

        _laporanListView.setAdapter(adapter);
    }

    private void getSum(String[] params, DBHandler db){
        //mencari stok
        StringBuilder query = new StringBuilder();
        query.append(" SELECT");
        query.append(" SUM(l."+Laporan.KEY_STOK+") as "+Laporan.KEY_STOK+", SUM(l."+Laporan.KEY_LABA_RUGI+") as "+Laporan.KEY_LABA_RUGI);
        query.append(" FROM "+Laporan.TABLE_NAME +" as l");
        query.append(" INNER JOIN "+ Outlet.TABLE_NAME+" as o ON o."+Outlet.KEY_ID+"=l."+Laporan.KEY_OUTLET_ID);
        query.append(" WHERE");
        query.append(" l."+Laporan.KEY_ISACTIVE+" = 1 AND o."+Outlet.KEY_ISACTIVE+"=1");
        int count = params.length;
        if(params.length > 0){
            query.append(" AND l."+Laporan.KEY_CREATE_DATE +" >= '"+params[0]+"' AND l."+Laporan.KEY_CREATE_DATE +" <= '"+params[1]+"'");
        }else{
            String date = Helper.formatDate("yyyy-MM-dd", new Date());
            query.append(" AND l."+Laporan.KEY_CREATE_DATE +" >= '"+date+"' AND l."+Laporan.KEY_CREATE_DATE +" <= '"+date+"'");
        }
        query.append(" ORDER BY l."+Laporan.KEY_CREATE_DATE);

        ArrayList<HashMap<String,String>> sum = db.selectList(new String[]{Outlet.KEY_STOK, Laporan.KEY_LABA_RUGI}, query.toString());
        sumStok = sum.get(0).get(Laporan.KEY_STOK);
        if("".equalsIgnoreCase(sumStok) || sumStok == null){
            sumStok = "0";
        }
        sumLabaRugi = sum.get(0).get(Laporan.KEY_LABA_RUGI);
        if("".equalsIgnoreCase(sumLabaRugi) || sumLabaRugi == null){
            sumLabaRugi = "0";
        }
    }
}

