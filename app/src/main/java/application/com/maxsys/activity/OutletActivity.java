package application.com.maxsys.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import application.com.maxsys.Helper;
import application.com.maxsys.R;
import application.com.maxsys.database.DBHandler;
import application.com.maxsys.handler.Laporan;
import application.com.maxsys.handler.Outlet;
import application.com.maxsys.handler.Product;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by KARSA on 31/05/2016.
 */
public class OutletActivity extends AppCompatActivity {
    @Bind(R.id.listView_outlet)
    ListView _outletListview;

    ProgressDialog dialog;
    private DatePickerDialog dialogDateFrom;
    private DatePickerDialog dialogDateTo;
    Outlet outlet = new Outlet();
    Menu menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_outlet);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _outletListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<HashMap<String, String>> listData = outlet.get_listData();
                HashMap<String, String> data = listData.get(position);

                Outlet editOutlet = new Outlet();
                editOutlet.parse(data);

                showUpdatePopup(editOutlet);
            }
        });

        GetOutlet getOutlet = new GetOutlet();
        getOutlet.execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_outlet, menu);
        this.menu = menu;
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case R.id.btn_add_outlet:
                showCreatePopup();
                break;
            case android.R.id.home:
                finish();
                break;
            case R.id.btn_filter:
                showDateDialog();
                break;
            case R.id.btn_empty_filter:
                GetOutlet getOutlet = new GetOutlet();
                getOutlet.execute();
                menu.getItem(1).setVisible(false);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    class GetOutlet extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>>{
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(OutletActivity.this);
            dialog.setMessage("Memuat list...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected ArrayList<HashMap<String,String>> doInBackground(String... params) {
            DBHandler db = new DBHandler(OutletActivity.this);

            ArrayList<HashMap<String,String>> data = db.selectList(Outlet.TABLE_NAME, Outlet.allColumn());

            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String,String>> s) {
            setDataList(s);
            dialog.dismiss();
        }
    }

    private void setDataList(ArrayList<HashMap<String,String>> data){
        outlet.parse(data);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1, outlet.get_listNama());

        _outletListview.setAdapter(adapter);
    }

    private void showCreatePopup() {
        // We need to get the instance of the LayoutInflater
        LayoutInflater inflater = (LayoutInflater) OutletActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_outlet, (ViewGroup) findViewById(R.id.popup_outlet));

        AlertDialog.Builder pw = new AlertDialog.Builder(OutletActivity.this);
        pw.setView(layout);
        pw.setCancelable(false);

        final EditText _namaInput = (EditText) layout.findViewById(R.id.input_nama_outlet);
        final EditText _alamatInput = (EditText) layout.findViewById(R.id.input_alamat);
        final EditText _telpInput = (EditText) layout.findViewById(R.id.input_telp);
        final EditText _stokInput = (EditText) layout.findViewById(R.id.input_stok);
        final CheckBox _isactiveCheck = (CheckBox) layout.findViewById(R.id.check_isactive);
        final Spinner _hargaInput = (Spinner) layout.findViewById(R.id.list_harga);

        DBHandler db = new DBHandler(this);
        final HashMap<String, String> listHash = db.select(Product.TABLE_NAME, new String[]{Product.KEY_HARGA1, Product.KEY_HARGA2,
                Product.KEY_HARGA3, Product.KEY_HPP, Product.KEY_STOK}, Product.KEY_ID + "=" + Product.PRODUCT_ID);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1, Product.convertCostList(listHash));
        _hargaInput.setAdapter(adapter);

        pw.setPositiveButton(getString(R.string.btn_simpan), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (validate(_namaInput, _alamatInput, _stokInput)) {
                    DBHandler db = new DBHandler(OutletActivity.this);
                    ContentValues values = Outlet.createContentValue(
                            _namaInput.getText().toString()
                            ,_alamatInput.getText().toString()
                            , _telpInput.getText().toString()
                            , _hargaInput.getSelectedItemPosition()
                            , _hargaInput.getSelectedItem().toString()
                            , _stokInput.getText().toString()
                            , "0"
                            , _isactiveCheck.isChecked()
                            , Product.PRODUCT_ID
                            , new Date()
                            , new Date());

                    db.insert(Outlet.TABLE_NAME, values);
                    long outletId = db.id;

                    //update product
                    ContentValues value = new ContentValues();
                    int stokOld = Integer.parseInt(listHash.get(Product.KEY_STOK));
                    int stokSubstract = Integer.parseInt(_stokInput.getText().toString());

                    int stokNow = stokOld - stokSubstract;
                    value.put(Product.KEY_STOK, stokNow);
                    value.put(Product.KEY_UPDATE_DATE, Helper.formatDate("yyyy-MM-dd", new Date()));

                    db.update(Product.TABLE_NAME, value, Product.KEY_ID + "=" + Product.PRODUCT_ID);

//                    //create laporan
//                    int hpp = Laporan.calculateHpp(Integer.parseInt(_stokInput.getText().toString()), Integer.parseInt(listHash.get(Product.KEY_HPP)));
//                    int profit = Laporan.calculateProfit(Integer.parseInt(_stokInput.getText().toString()), Integer.parseInt(listHash.get(Product.KEY_HPP)),
//                            Integer.parseInt(_hargaInput.getSelectedItem().toString()));
//
//                    db.insert(Laporan.TABLE_NAME, Laporan.createQuery(hpp, profit, 0,new Date(), new Date(), Product.PRODUCT_ID, outletId, false));

                    dialog.dismiss();

                    GetOutlet getOutlet = new GetOutlet();
                    getOutlet.execute();
                } else {
                    Toast.makeText(OutletActivity.this, "Data anda gaagl disimpan silakan ulangi input data outlet", Toast.LENGTH_SHORT).show();
                }
            }
        });

        pw.setNegativeButton(getString(R.string.btn_tidak), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        pw.show();
    }

    private void showUpdatePopup(final Outlet editOutlet) {
        final int id = editOutlet.getId();
        String nama = editOutlet.getNama();
        String alamat = editOutlet.getAlamat();
        String telp = editOutlet.getTelp();
        final int stokOld = editOutlet.getStok();
        int indexHarga = editOutlet.getIndexHarga();
        boolean active = editOutlet.getIsActive();

        // We need to get the instance of the LayoutInflater
        LayoutInflater inflater = (LayoutInflater) OutletActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_outlet, (ViewGroup) findViewById(R.id.popup_outlet));

        AlertDialog.Builder pw = new AlertDialog.Builder(OutletActivity.this);
        pw.setCancelable(false);
        pw.setView(layout);

        DBHandler db = new DBHandler(this);
        final HashMap<String, String> listHash = db.select(Product.TABLE_NAME, new String[]{Product.KEY_HARGA1, Product.KEY_HARGA2, Product.KEY_HARGA3,Product.KEY_HPP, Product.KEY_STOK}
                , Product.KEY_ID + "=" + Product.PRODUCT_ID);
        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1, Product.convertCostList(listHash));

        final EditText _namaInput = (EditText) layout.findViewById(R.id.input_nama_outlet);
        _namaInput.setText(nama);
        final EditText _alamatInput = (EditText) layout.findViewById(R.id.input_alamat);
        _alamatInput.setText(alamat);
        final EditText _telpInput = (EditText) layout.findViewById(R.id.input_telp);
        _telpInput.setText(telp);
        final EditText _stokInput = (EditText) layout.findViewById(R.id.input_stok);
        _stokInput.setText(String.valueOf(stokOld));
        final Spinner _hargaInput = (Spinner) layout.findViewById(R.id.list_harga);
        _hargaInput.setAdapter(adapter);
        _hargaInput.setSelection(indexHarga);
        final CheckBox _isactiveCheck = (CheckBox) layout.findViewById(R.id.check_isactive);
        _isactiveCheck.setChecked(active);

        pw.setNegativeButton(getString(R.string.btn_tidak), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        pw.setPositiveButton(R.string.btn_simpan, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String updateDate = Helper.formatDate("yyyy-MM-dd" , editOutlet.getUpdateDate());
                String dateNow = Helper.formatDate("yyyy-MM-dd", new Date());
                String stok  = _stokInput.getText().toString();
                int retur = editOutlet.getStokRetur();

                if(Integer.parseInt(stok) != stokOld){
                    if(!dateNow.equalsIgnoreCase(updateDate)){
                        if(retur > 0){
                            retur = 0;
                        }else{
                            Toast.makeText(getApplicationContext(), "Tidak bisa update stok karena sudah masih memiliki order yang belum selesai", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    }else if( retur > 0 ){
                        Toast.makeText(getApplicationContext(), "Tidak bisa update stok karena sudah memiliki laporan", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                if (validate(_namaInput, _alamatInput, _stokInput)) {
                    DBHandler db = new DBHandler(OutletActivity.this);
                    ContentValues values = new ContentValues();
                    values.put(Outlet.KEY_NAMA, _namaInput.getText().toString());
                    values.put(Outlet.KEY_ALAMAT, _alamatInput.getText().toString());
                    values.put(Outlet.KEY_TELP, _telpInput.getText().toString());
                    values.put(Outlet.KEY_STOK, stok);
                    values.put(Outlet.KEY_STOK_RETUR, retur);
                    values.put(Outlet.KEY_INDEX_HARGA, _hargaInput.getSelectedItemPosition());
                    values.put(Outlet.KEY_HARGA, _hargaInput.getSelectedItem().toString());
                    values.put(Outlet.KEY_ISACTIVE, _isactiveCheck.isChecked());
                    values.put(Outlet.KEY_PRODUCT_ID, Product.PRODUCT_ID);
                    values.put(Outlet.KEY_UPDATE_DATE, dateNow);

                    db.update(Outlet.TABLE_NAME, values, Outlet.KEY_ID + "=" + id);

                    //update product
                    ContentValues value = new ContentValues();
                    int stokOldProduct = Integer.parseInt(listHash.get(Product.KEY_STOK));
                    int stokSubstract = stokOld - Integer.parseInt(_stokInput.getText().toString());

                    int stokNow = stokOldProduct + stokSubstract;
                    value.put(Product.KEY_STOK, stokNow);
                    value.put(Product.KEY_UPDATE_DATE, Helper.formatDate("yyyy-MM-dd", new Date()));

                    db.update(Product.TABLE_NAME, value, Product.KEY_ID + "=" + Product.PRODUCT_ID);
                    dialog.dismiss();

                    GetOutlet getOutlet = new GetOutlet();
                    getOutlet.execute();
                }
            }
        });

        pw.show();
    }

    private boolean validate(EditText _namaInput, EditText _alamatInput, EditText _stokInput){
        boolean result = true;
        String message = "";
        String nama = _namaInput.getText().toString();
        String alamat = _alamatInput.getText().toString();
        String stok = _stokInput.getText().toString();

        if(nama.isEmpty() ||  "".equalsIgnoreCase(nama)){
            message = "Nama outlet harus diisi";
            _namaInput.setError(message);
            result = false;
        }

        if(alamat.isEmpty() || "".equalsIgnoreCase(alamat)){
            message = "Alamat toko harus diisi";
            _alamatInput.setError(message);
            if(!result){
                result = false;
            }
        }

        if(stok.isEmpty() || "".equalsIgnoreCase(stok)){
            message = "Stok harus diisi";
            _stokInput.setError(message);
            if(!result){
                result = false;
            }
        }

        if(!result){
            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        }

        return result;
    }

    private void showDateDialog(){
        LayoutInflater inflater = (LayoutInflater) OutletActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_filter, (ViewGroup) findViewById(R.id.popup_filter));

        AlertDialog.Builder alert = new AlertDialog.Builder(OutletActivity.this);
        alert.setView(layout);

        final EditText _dateFromInput = (EditText) layout.findViewById(R.id.input_date_from);
        final EditText _dateToInput = (EditText) layout.findViewById(R.id.input_date_to);
        final EditText _outletNameInput = (EditText) layout.findViewById(R.id.input_outlet_name);
        _outletNameInput.setVisibility(View.VISIBLE);
        Button _dateToBtn = (Button) layout.findViewById(R.id.btn_date_to);
        Button _dateFromBtn = (Button) layout.findViewById(R.id.btn_date_from);

        setData(_dateFromInput, _dateToInput);

        _dateFromBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDateFrom.show();
            }
        });

        _dateToBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDateTo.show();
            }
        });

        alert.setPositiveButton(R.string.btn_filter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MenuItem item = menu.getItem(1);
                item.setVisible(true);

                FilterLaporan filter = new FilterLaporan();
                filter.execute(new String[]{_dateFromInput.getText().toString(),
                        _dateToInput.getText().toString(), _outletNameInput.getText().toString()});
            }
        });

        alert.setNegativeButton(R.string.btn_tidak, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.show();
    }

    private void setData(final EditText _dateFrom, final EditText _dateTo){
        Calendar calendar = Calendar.getInstance();

        dialogDateFrom = new DatePickerDialog(OutletActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                String date = Helper.formatDate("yyyy-MM-dd",newDate.getTime());
                _dateFrom.setText(date);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        dialogDateTo = new DatePickerDialog(OutletActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                String date = Helper.formatDate("yyyy-MM-dd",newDate.getTime());
                _dateTo.setText(date);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    class FilterLaporan extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>> {
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(OutletActivity.this);
            dialog.setMessage("Memuat list...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected ArrayList<HashMap<String,String>> doInBackground(String... params) {
            DBHandler db = new DBHandler(OutletActivity.this);

            boolean firstWhere = true;
            StringBuilder whereClause = new StringBuilder();

            if(!("".equalsIgnoreCase(params[0]) || params[0] == null)){
                whereClause.append(Outlet.KEY_CREATE_DATE + ">=" + params[0]);
                firstWhere = false;
            }

            if(!("".equalsIgnoreCase(params[1]) || params[1] == null)){
                if(!firstWhere){
                    whereClause.append(" AND ");
                }
                whereClause.append(Outlet.KEY_CREATE_DATE + "<=" + params[1]);
                firstWhere = false;
            }

            if(!("".equalsIgnoreCase(params[2]) || params[2] == null)){
                if(!firstWhere){
                    whereClause.append(" AND ");
                }
                whereClause.append(Outlet.KEY_NAMA + " LIKE '%"+params[2]+"%'" );
                firstWhere = false;
            }

            ArrayList<HashMap<String,String>> data = db.selectList(Outlet.TABLE_NAME, Outlet.allColumn(), whereClause.toString());

            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String,String>> data){
            setDataList(data);
            dialog.dismiss();
        }
    }
}

