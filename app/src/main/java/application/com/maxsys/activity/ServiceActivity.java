package application.com.maxsys.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.BitmapDrawable;
import android.os.AsyncTask;
import  android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import application.com.maxsys.Helper;
import application.com.maxsys.R;
import application.com.maxsys.database.DBHandler;
import application.com.maxsys.handler.Laporan;
import application.com.maxsys.handler.Outlet;
import application.com.maxsys.handler.Product;
import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by KARSA on 31/05/2016.
 */
public class ServiceActivity extends AppCompatActivity {
    @Bind(R.id.listView_service)
    ListView _serviceListview;

    ProgressDialog dialog;
    Outlet outlet = new Outlet();
    Menu menu;
    private DatePickerDialog dialogDateFrom;
    private DatePickerDialog dialogDateTo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        _serviceListview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ArrayList<HashMap<String, String>> listData = outlet.get_listData();
                HashMap<String, String> data = listData.get(position);

                Outlet editOutlet = new Outlet();
                editOutlet.parse(data);

                showUpdatePopup(editOutlet.getId(), editOutlet.getStok(), editOutlet.getStokRetur(), editOutlet.getHarga(), editOutlet.getNama());
            }
        });

        GetOutlet getOutlet = new GetOutlet();
        getOutlet.execute();
    }

    class GetOutlet extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>> {
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(ServiceActivity.this);
            dialog.setMessage("Memuat list...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected ArrayList<HashMap<String,String>> doInBackground(String... params) {
            DBHandler db = new DBHandler(ServiceActivity.this);

            // mencari yang outlet yang aktif saja
            ArrayList<HashMap<String,String>> data = db.selectList(Outlet.TABLE_NAME, Outlet.allColumn(), Outlet.KEY_ISACTIVE+"="+1);

            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String,String>> data) {
            setDataList(data);
            dialog.dismiss();
        }
    }

    private void setDataList(ArrayList<HashMap<String,String>> data){
        outlet.parse(data);

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1, outlet.get_listNama());

        _serviceListview.setAdapter(adapter);
    }

    private void showUpdatePopup(final int id, final int stokOld, final int stokReturOld, final int hargaOutlet, final String nama) {
        // We need to get the instance of the LayoutInflater
        LayoutInflater inflater = (LayoutInflater) ServiceActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_outlet, (ViewGroup) findViewById(R.id.popup_outlet));

        AlertDialog.Builder pw = new AlertDialog.Builder(ServiceActivity.this);
        pw.setCancelable(false);
        pw.setView(layout);

        DBHandler db = new DBHandler(this);
        final HashMap<String, String> listHash = db.select(Product.TABLE_NAME,
                new String[]{Product.KEY_HPP, Product.KEY_HARGA1, Product.KEY_HARGA2, Product.KEY_HARGA3, Product.KEY_STOK}
                , Product.KEY_ID + "=" + Product.PRODUCT_ID);

        final TextView _outletText = (TextView) layout.findViewById(R.id.text_name_outlet);
        _outletText.setText(nama);
        _outletText.setVisibility(View.VISIBLE);
        final EditText _namaInput = (EditText) layout.findViewById(R.id.input_nama_outlet);
        _namaInput.setVisibility(View.GONE);
        final EditText _alamatInput = (EditText) layout.findViewById(R.id.input_alamat);
        _alamatInput.setVisibility(View.GONE);
        final EditText _telpInput = (EditText) layout.findViewById(R.id.input_telp);
        _telpInput.setVisibility(View.GONE);
        final CheckBox _isactiveCheck = (CheckBox) layout.findViewById(R.id.check_isactive);
        _isactiveCheck.setVisibility(View.GONE);
        final LinearLayout _spinnerLayout = (LinearLayout) layout.findViewById(R.id.layout_spinner);
        _spinnerLayout.setVisibility(View.GONE);
        final Spinner _hargaInput = (Spinner) layout.findViewById(R.id.list_harga);
        _hargaInput.setVisibility(View.GONE);
        final EditText _stokInput = (EditText) layout.findViewById(R.id.input_stok);
        _stokInput.setText(String.valueOf(stokOld));
        final EditText _stokReturInput = (EditText) layout.findViewById(R.id.input_stok_retur);
        _stokReturInput.setVisibility(View.VISIBLE);
        _stokReturInput.setText(String.valueOf(stokReturOld));

        _stokInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                int stok = 0;
                if(!"".equalsIgnoreCase(_stokInput.getText().toString())){
                    stok = Integer.parseInt(_stokInput.getText().toString());
                }

                int stokRetur = stokOld - stok + stokReturOld;// 100 - 50 - 0 = 50 || 50 - 50 + 50 = 50
                _stokReturInput.setText(String.valueOf(stokRetur));//50
            }
        });

        pw.setNegativeButton(R.string.btn_tidak, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        pw.setPositiveButton(R.string.btn_simpan, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if("".equalsIgnoreCase(_stokInput.getText().toString())){
                    _stokInput.setText("0");
                }
                int stokRetur = Integer.parseInt(_stokReturInput.getText().toString());//50

                if(validate(stokOld, stokRetur, stokReturOld)){
                    DBHandler db = new DBHandler(ServiceActivity.this);
                    ContentValues values = new ContentValues();

                    int stokOutlet = stokOld + stokReturOld - stokRetur;//50 + 0 - 17 = 33
                    values.put(Outlet.KEY_STOK, stokOutlet);// 50
                    values.put(Outlet.KEY_STOK_RETUR, stokRetur);// 50

                    db.update(Outlet.TABLE_NAME, values, Outlet.KEY_ID + "=" + id);

                    //create laporan
                    String hpp = listHash.get(Product.KEY_HPP);//3000
                    int hppProduct = Integer.parseInt(hpp);//3000
                    int hppLaporan = Laporan.calculateHpp(stokOutlet, hppProduct);//99000
                    int profitLaporan = Laporan.calculateProfit(stokOutlet, hppProduct, hargaOutlet);//

                    ContentValues valueLaporan = new ContentValues();
                    valueLaporan.put(Laporan.KEY_HPP, hppLaporan);
                    valueLaporan.put(Laporan.KEY_LABA_RUGI, profitLaporan);
                    valueLaporan.put(Laporan.KEY_STOK, stokOutlet);
                    valueLaporan.put(Laporan.KEY_ISACTIVE, true);

                    if(stokReturOld > 0){
                        String datefrom = Helper.formatDate("yyyy-MM-dd", new Date());
                        String dateTo = Helper.formatDate("yyyy-MM-dd", Helper.addDay(new Date(), 1));

                        db.update(Laporan.TABLE_NAME, valueLaporan, Laporan.KEY_OUTLET_ID + "=" + id +" AND "
                                +Laporan.KEY_CREATE_DATE+">='"+datefrom+"'"+" AND "+Laporan.KEY_CREATE_DATE+"<='"+dateTo+"'");
                    }else{
                        db.insert(Laporan.TABLE_NAME, Laporan.createQuery(hppLaporan, profitLaporan, stokOutlet, new Date(), new Date(), Product.PRODUCT_ID, id, true));
                    }

                    //update product
                    ContentValues value = new ContentValues();
                    int stokOldProduct = Integer.parseInt(listHash.get(Product.KEY_STOK));

                    int stokNow = stokOldProduct + stokRetur - stokReturOld ;//900 + 50 - 0 = 950 || 950 + 75 - 50
                    value.put(Product.KEY_STOK, stokNow);
                    value.put(Product.KEY_UPDATE_DATE, Helper.formatDate("yyyy-MM-dd", new Date()));

                    db.update(Product.TABLE_NAME, value, Product.KEY_ID + "=" + Product.PRODUCT_ID);
                    dialog.dismiss();

                    GetOutlet getOutlet = new GetOutlet();
                    getOutlet.execute();
                }
            }
        });

        pw.show();
    }

    private boolean validate(int stokOld, int stokRetur, int stokReturOld){
        boolean result = true;

        int stok = stokOld + stokReturOld;

        if(stokRetur > stok){
            Toast.makeText(ServiceActivity.this, "Stok yang kembali melebihi stok order", Toast.LENGTH_SHORT).show();
            result = false;
        }else if(stokRetur < 0){
            Toast.makeText(getApplicationContext(), "Stok  retur melebihi stok awal", Toast.LENGTH_SHORT).show();
            result = false;
        }

        return result;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_outlet, menu);
        this.menu = menu;
        menu.getItem(0).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id){
            case android.R.id.home:
                finish();
                break;
            case R.id.btn_filter:
                showDateDialog();
                break;
            case R.id.btn_empty_filter:
                GetOutlet getOutlet = new GetOutlet();
                getOutlet.execute();
                menu.getItem(1).setVisible(false);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void showDateDialog(){
        LayoutInflater inflater = (LayoutInflater) ServiceActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = inflater.inflate(R.layout.popup_filter, (ViewGroup) findViewById(R.id.popup_filter));

        AlertDialog.Builder alert = new AlertDialog.Builder(ServiceActivity.this);
        alert.setView(layout);

        final EditText _dateFromInput = (EditText) layout.findViewById(R.id.input_date_from);
        final EditText _dateToInput = (EditText) layout.findViewById(R.id.input_date_to);
        final EditText _outletNameInput = (EditText) layout.findViewById(R.id.input_outlet_name);
        _outletNameInput.setVisibility(View.VISIBLE);
        Button _dateToBtn = (Button) layout.findViewById(R.id.btn_date_to);
        Button _dateFromBtn = (Button) layout.findViewById(R.id.btn_date_from);

        setData(_dateFromInput, _dateToInput);

        _dateFromBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDateFrom.show();
            }
        });

        _dateToBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogDateTo.show();
            }
        });

        alert.setPositiveButton(R.string.btn_filter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MenuItem item = menu.getItem(1);
                item.setVisible(true);

                FilterLaporan filter = new FilterLaporan();
                filter.execute(new String[]{_dateFromInput.getText().toString(),
                        _dateToInput.getText().toString(), _outletNameInput.getText().toString()});
            }
        });

        alert.setNegativeButton(R.string.btn_tidak, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.show();
    }

    private void setData(final EditText _dateFrom, final EditText _dateTo){
        Calendar calendar = Calendar.getInstance();

        dialogDateFrom = new DatePickerDialog(ServiceActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                String date = Helper.formatDate("yyyy-MM-dd",newDate.getTime());
                _dateFrom.setText(date);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        dialogDateTo = new DatePickerDialog(ServiceActivity.this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);

                String date = Helper.formatDate("yyyy-MM-dd",newDate.getTime());
                _dateTo.setText(date);
            }
        }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
    }

    class FilterLaporan extends AsyncTask<String, Void, ArrayList<HashMap<String,String>>> {
        @Override
        protected void onPreExecute() {
            dialog = new ProgressDialog(ServiceActivity.this);
            dialog.setMessage("Memuat list...");
            dialog.setCancelable(false);
            dialog.show();
        }

        @Override
        protected ArrayList<HashMap<String,String>> doInBackground(String... params) {
            DBHandler db = new DBHandler(ServiceActivity.this);

            StringBuilder whereClause = new StringBuilder();

            whereClause.append(Outlet.KEY_ISACTIVE+"=1 ");

            if(!("".equalsIgnoreCase(params[0]) || params[0] == null)){
                whereClause.append(" AND "+Outlet.KEY_CREATE_DATE + ">=" + params[0]);
            }

            if(!("".equalsIgnoreCase(params[1]) || params[1] == null)){
                whereClause.append(" AND "+Outlet.KEY_CREATE_DATE + "<=" + params[1]);
            }

            if(!("".equalsIgnoreCase(params[2]) || params[2] == null)){
                whereClause.append(" AND "+Outlet.KEY_NAMA + " LIKE '%"+params[2]+"%'" );
            }

            ArrayList<HashMap<String,String>> data = db.selectList(Outlet.TABLE_NAME, Outlet.allColumn(), whereClause.toString());

            return data;
        }

        @Override
        protected void onPostExecute(ArrayList<HashMap<String,String>> data){
            setDataList(data);
            dialog.dismiss();
        }
    }
}
